From alpine:latest
Add Helloworld.class Helloworld.class
RUN apk --update add openjdk8-jre
ENTRYPOINT ["java" "-Djava.security.egd=file:/dev/.urandom","Helloworld"]
